var gulp = require('gulp');
var gutil = require('gulp-util');
var source = require('vinyl-source-stream'); // Converts Browserify stream to a format that can be consumed by other Gulp plugins
var browserify = require('browserify');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
// var jshint = require('gulp-jshint');
// var stylish = require('jshint-stylish');
// var uglify = require('gulp-uglify');
var inject = require('gulp-inject');
var fileinclude = require('gulp-file-include');
var notify = require("gulp-notify");
var browserSync = require('browser-sync').create();
var replace = require('gulp-replace');
//var relativePathPrefix = '/sites/$CLIENT'; // ***UPDATE THIS TO $CLIENT SITE***

// Compile Sass ==============================================
gulp.task('css', function () {
  return gulp.src('./css/**/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(plumber())
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(concat('app.css'))
    .pipe(sourcemaps.write('.'))
    // .pipe(sourcemaps.write()) // Inline
    .pipe(gulp.dest('./_catalogs/masterpage/assets/css'))
    //.pipe(notify({ message: "Sass compiled"}) )
    .pipe(browserSync.stream());
});

gulp.task('inject', function() {
  return gulp.src('./prototype/src/**/*.html')
    // Includes ==============================================
    .pipe(fileinclude({
      prefix: '@@',
      basepath: './prototype/includes'
    }))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "File include tasks done."}) )
    // Inject JS ==============================================
    .pipe(inject(gulp.src(['./_catalogs/masterpage/assets/js/vendor.js','./_catalogs/masterpage/assets/js/app.js'],{ read: false }), { relative: true }))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "JS injected to layouts." }))
    // Inject CSS ==============================================
    .pipe(inject(
      gulp.src('./_catalogs/masterpage/assets/css/app.css', { read: false }), { relative: true }))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "CSS injected to layouts." }))
    .pipe(browserSync.stream());
});

gulp.task('inject-master', function() {
  return gulp.src('./prototype/src/**/*.master')
    // Includes ==============================================
    .pipe(fileinclude({
      prefix: '@@',
      basepath: './prototype/includes'
    }))
    .pipe(replace('"/_catalogs/masterpage/assets', function(match) {
      // Replaces instances of "foo" with "oof" 
      return '"' + relativePathPrefix +'/_catalogs/masterpage/assets';
    }))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "File include tasks done."}) )
    // Inject JS ==============================================
    // .pipe(inject(gulp.src(['./_catalogs/masterpage/assets/js/vendor.js','./_catalogs/masterpage/assets/js/app.js'],{ read: false }), { relative: true }))
    .pipe(gulp.dest('./_catalogs/masterpage/assets'))
    // .pipe(notify({ message: "JS injected to masterpage." }))
    // Inject CSS ==============================================
    // .pipe(inject( gulp.src('./_catalogs/masterpage/assets/css/app.css', { read: false }), { relative: true }))
    // .pipe(gulp.dest('./_catalogs/masterpage/assets'))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "CSS injected to masterpage." }))
    .pipe(browserSync.stream());
});

gulp.task('inject-layouts', function() {
  return gulp.src('./prototype/src/**/*.aspx')
    // Includes ==============================================
    .pipe(fileinclude({
      prefix: '@@',
      basepath: './prototype/includes'
    }))
    .pipe(replace('"/_catalogs/masterpage/assets', function(match) {
      // Replaces instances of "foo" with "oof" 
      return '"' + relativePathPrefix +'/_catalogs/masterpage/assets';
    }))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "File include tasks done."}) )
    // Inject JS ==============================================
    // .pipe(inject(gulp.src(['./_catalogs/masterpage/assets/js/vendor.js','./_catalogs/masterpage/assets/js/app.js'],{ read: false }), { relative: true }))
    .pipe(gulp.dest('./_catalogs/masterpage/PageLayouts'))
    // .pipe(notify({ message: "JS injected to masterpage." }))
    // Inject CSS ==============================================
    // .pipe(inject( gulp.src('./_catalogs/masterpage/assets/css/app.css', { read: false }), { relative: true }))
    // .pipe(gulp.dest('./_catalogs/masterpage/assets'))
    .pipe(gulp.dest('./prototype/dist'))
    // .pipe(notify({ message: "CSS injected to masterpage." }))
    .pipe(browserSync.stream());
});
// BrowserSync ==============================================
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

// Serve up local files, initialize Watch Task ==============================================
gulp.task('serve', function() {
    gulp.start('css', 'inject', 'browser-sync', 'watch');
});
// Build local files,inject includes and js ==============================================
gulp.task('build', function() {
    gulp.start('css', 'inject');
});

// Watch  ==============================================
gulp.task('watch', function() {
  // CSS 
  gulp.watch('./css/**/*.scss', ['css']);
  // HTML
  gulp.watch('./prototype/**/*.html', ['inject']);
  gulp.watch('./prototype/includes/**/*.html', ['inject']);
});