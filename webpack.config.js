const path = require('path')
const webpack = require('webpack')
const pkg = require('./package.json');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const PRODUCTION_MODE = process.env.NODE_ENV === 'production'

module.exports = {
    devtool: PRODUCTION_MODE ? false : 'source-map',
    devServer: undefined,

  /**
   * Webpack entry points: add files to this array if you'd like them to be
   * compiled and copied to the build folder using the modules in this
   * configuration file
   */
   entry: {
    app: [
      './app/app.js',
    ],
    vendor: Object.keys(pkg.dependencies),
   // branding: [
      //'./js/branding.js'
      //,
     // './css/app.scss'
    //]
  },

  /**
   * Webpack output configuration: be sure to update this path if you decide to
   * rename your theme directory
   */
  output: {
    path: path.resolve(__dirname, './_catalogs/masterpage/assets/js'),
    filename: '[name].js'
  },

  /**
   * Use source-map to create source maps during development builds
   */
  devtool: PRODUCTION_MODE ? false : 'source-map',

  /**
   * Webpack module configuration
   */
    module: {   
    rules: [

      /**
       * Compile ES6 to JavaScript in the build folder using babel-loader
       */
     {
                test: /\.html$/,
                exclude: /node_modules/,
                loader: "raw-loader"
            },
           
      {
        test: /\.s[ac]ss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: !PRODUCTION_MODE
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: !PRODUCTION_MODE
              }
            }
          ]
        })
      },

      /**
       * Compile ES6 to JavaScript in the build folder using babel-loader
       */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              'env'
            ]
          }
        }
      }

    ]
    },
    
    /**
    * Additional configuration of Webpack plugins
   */
    plugins: [
      
      /**
       * Default output path for compiled CSS files
       */
        //new ExtractTextPlugin('../css/[name].css'),

        new webpack.optimize.UglifyJsPlugin()
    ]
};